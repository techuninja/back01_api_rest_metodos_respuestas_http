package com.techu.apirestprod.models;

import java.util.List;

public class ProductoModel {

    private long id;
    private String descripcion;
    private double precio;
    private List<UserModel> users;

    public ProductoModel() {
    }

    public ProductoModel(long id, String descripcion, double precio) {
        this.id = id;
        this.descripcion = descripcion;
        this.precio = precio;
    }

    public ProductoModel(long id, String descripcion, double precio, List<UserModel> users) {
        this.id = id;
        this.descripcion = descripcion;
        this.precio = precio;
        this.users = users;
    }

    public long getId() {
        return id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public List<UserModel> getUsers(){
        return this.users;
    }

    public void setUsers(List<UserModel> users) {
        this.users = users;
    }
}
