package com.techu.apirestprod;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApirestprodApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApirestprodApplication.class, args);
    }

}
